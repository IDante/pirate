<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inventory;
use App\Models\Employee;
use App\Models\Client;
use App\Models\Rent;
use App\Models\Director;
use App\Models\User;
use Auth;
use Validator;
use DateTime;


class InventoryController extends Controller
{

    public function create()
    {
        $item = Auth::user();
       return view('employee.create', compact('item'));
    }
    //-------------------------------CRUD--------------------------------------------

    /**
     * Create an Inventory
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        
        
        //dd ($request->input());
        if(!Employee::where('user_id', Auth::user()->id))
        {
            return response()->json(['message' => 'Error'], 509);
        }

        $g = Inventory::orderBy('id', 'desc')->first();
        if ($g == null) $id = 1;
        else $id = $g->id + 1;

      
       
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'week_price' => 'numeric|required|nullable',
            'day_price' => 'numeric|required|nullable',
        ]);


        
        $inventory = Inventory::create(array_merge(
            $validator->validated(),
            ['id' => $id,
             'employee_id' => Employee::where('user_id',Auth::user()->id)->first()->id,
             ]
        ));

        if($inventory)
        {
            return redirect(route('inventory.getall', 'all'));
        }
    }



    public function Edit_s($id)
    {
        $item = Inventory::find($id);
        $dates = Rent::where('inventory_id', $id)->get();
        
        return view('employee.edit', compact('item', 'dates'));
    }

    /**
     * Edit an Inventory
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function Edit(Request $request, $id)
    {
        
        // if (!Auth::guard('api')->check()) {
        //     return response()->json(['message' => 'Error'], 401);
        // }

       
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'week_price' => 'numeric',
            'day_price' => 'numeric',
        ]);

        $inventory = Inventory::find($id);
        
        

        $inventory->fill(array_merge(
            $validator->validated(),
        ));

     
        $inventory->save();

        return redirect(route('inventory.getall', 'all'));
        // return response()->json([
        //     $inventory,
        //     'message' => 'Inventory has been modified successfully',
        // ], 201);

    }



    /**
     * Delete an Inventory
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    static public function delete($id)
    {
        //User::registrations()->delete($id);return 1;

        $inventory = Inventory::where('id', $id)->first();

       
        if(sizeof(Rent::all()->where('inventory_id', $id)) != 0)
        {
            return response()->json([
            'message' => 'Cannot delete this inventory',
            ], 409);
        }

    
            $inventory->delete();
            return redirect()->back();
            //response()->json([
            //     'message' => 'inventory deleted successfully',
            // ], 201);
        
    }      

    //-------------------------------CRUD--------------------------------------------


    public function make_emp($id)
    {
        Client::where('user_id',$id)->first()->delete();
        Employee::create([
            'user_id' => $id,
        ]);
        return redirect()->back();
    }


    public function get_all($status = 'all')
    {
        if($status == 'free')
        {
            $articles = Inventory::orderBy('id')->where('free', true)->get();
        }
        else if($status == 'all'){
            $articles = Inventory::orderBy('id')->get();
        }
       
        $now = new DateTime('NOW');
        
        foreach($articles as $article)
        {
            if($article->rent_end_time < $now->format('Y-m-d H:i:s') )
            {
                $article->rent_end_time = null;
                $article->rent_start_time = null;
                $article->free = true;
                $article->save();
            }
            $dates = Rent::where('inventory_id', $article->id)->get();
            foreach($dates as $date)
            {
                if($date->rent_start_time < $now->format('Y-m-d H:i:s'))
                {
                    $article->free = false;
                }
                else $article->free = true;
            }

            
        }

       
        
        
        if(Employee::where('user_id',Auth::id())->first() != null)
            return view('employee.index',['articles' => $articles, 'id' => Auth::id()]);
        else if (Director::where('user_id',Auth::id())->first() != null)
            return view('director.index',['articles' => $articles, 'id' => Auth::id()]);
        else return view('index',['articles' => $articles, 'id' => Auth::id()]);
    }
     

    public function statistics()
    {
        $emp = Employee::all();       
        $users = [];
        $money = 0;
        if(false) dd(1);

        foreach ($emp as $user)
        {
            foreach ($user->money as $mon){
                $money += $mon;
            }
            array_push($users, ['user' => User::find($user->user_id), 'money' => $money]);
            $money = 0;
        }
       
        

        return view('director.statistics', compact('users', 'emp'));
    }

    public function search(Request $request)
    {
        $emp = User::where('name', $request->name)->get();
        $users = [];
        $money = 0;

        foreach ($emp as $user)
        {
            foreach ($user->employee()->first()->money as $mon){
                $money += $mon;
            }
            array_push($users, ['user' => $user, 'money' => $money]);
            $money = 0;
        }
        
        

        return view('director.statistics', compact('users'));

    }


}

