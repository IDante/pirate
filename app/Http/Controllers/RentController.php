<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rent;
use Validator;
use \DateTime;
use App\Models\Inventory;
use App\Models\Employee;

use Auth;


class RentController extends Controller
{

    public function create($id)
    {
        
        $item = Inventory::find($id);
        $dates = Rent::where('inventory_id', $id)->get();
        
        //dd($item);
        return view('rent', compact('item', 'dates'));
       
    }

    public function store(Request $request)
    {
   
        
     
        $time  =new DateTime('NOW');
        
        $inventory = Inventory::find($request->inventory_id);


        // if($inventory->rent_end_time != null && $inventory->rent_end_time > $request->rent_start_time)
        // {
        //     return response()->json([
        //         'message' => 'Cannot rent this inventory',
        //         ], 409);
        // }

        $inventory->rent_start_time = date_create_from_format('m/d/Y', $request->from);

        //dd(gettype($inventory->rent_start_time->format('Y-m-d')));
      
        $inventory->rent_end_time = date_create_from_format('m/d/Y', $request->to);
        $inventory->free = false;


        $dates = Rent::where('inventory_id', $inventory->id)->get();

        foreach($dates as $date)
        {
            $data1 = date_create_from_format('Y-m-d H:i:s', $date->rent_start_time);
            //dd( $data1);
            $data2 = date_create_from_format('Y-m-d H:i:s', $date->rent__time);
            if($inventory->rent_start_time >= $data1 && $inventory->rent_start_time <=  $data2 )
            {
                $request->session()->flash('alert-success', 'Cannot rent');
                return 1;
            }
            if($inventory->rent_end_time >= $data1 && $inventory->rent_end_time <= $data2 )
            {
                $request->session()->flash('alert-success', 'Cannot rent');
                return 2;
            }
           // dd($data1 , $inventory->rent_start_time);
            if($data1 >= $inventory->rent_start_time && $data1 <= $inventory->rent_end_time)
            {
                $request->session()->flash('alert-success', 'Cannot rent');
                return redirect()->back();
            }
            if( $data2  >= $inventory->rent_start_time &&  $data2  <= $inventory->rent_end_time )
            {
                
                $request->session()->flash('alert-success', 'Cannot rent');
                return redirect()->back();
            }
        }



        $days = intval(date_diff( $inventory->rent_start_time, $inventory->rent_end_time)->format('%a'));

        //return $days;

       
        $price = $inventory->week_price * intdiv($days,7) + $inventory->day_price * ($days % 7);
        
        


        // if (!Auth::guard('api')->check()) {
        // return response()->json(['message' => 'Error'], 401);
        // }

        $g = Rent::orderBy('id', 'desc')->first();
        if ($g == null) $id = 1;
        else $id = $g->id + 1;

     
       

        $inventory->save();


    
       

        $employee = Employee::find($inventory->employee_id);
          
       $arr = $employee->money;
       
       if(array_key_exists($inventory->rent_start_time->format('Y-m-d'), $arr))
            $arr[$inventory->rent_start_time->format('Y-m-d')] += $price;
        else $arr[$inventory->rent_start_time->format('Y-m-d')] = $price;
        $employee->money = $arr;

        $employee->save();
        


        $rent = Rent::create(
            ['id' => $id,
                'inventory_id'=> $request->inventory_id,
                'client_id'=> 2,//Auth::id(),
                'employee_id'=> $inventory->employee_id,
                'price' => $price,
                'rent_start_time' => $inventory->rent_start_time,
                'rent_end_time' => $inventory->rent_end_time,

            ]
        );

        return redirect(route('inventory.getall', 'all')); 

    }


    static public function delete($id)
    {
        $rent = Rent::where('id', $id)->first();

        $inventory = Inventory::find($rent->inventory_id);

        $inventory->rent_start_time = null;
        $inventory->rent_end_time = null;
        $inventory->free = true;
        $inventory->save();

        $rent->delete();
        return redirect()->back(); 

    }
}      




