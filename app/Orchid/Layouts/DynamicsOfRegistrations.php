<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Chart;
use App\Models\Employee;

class DynamicsOfRegistrations extends Chart
{
   /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Statistics';
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'users';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

   
    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = true;

  
}
