<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Orchid\Layouts\DynamicsOfRegistrations;
use App\Models\User;

class Graphics extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Отчет';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    { 
        $emp = User::all();
        $users = [];
        $labels = [];
      
        $values = [];

        foreach ($emp as $user)
        {
           
        
            $empl = $user->employee()->first();
           
            if($empl != null)
            {
              
                foreach ($empl->money as $key => $value)
                {
                   
                    if(array_key_exists($key, $users))
                        $users[$key] += $value;  
                    else $users[$key] = $value; 
                }
            }
        }

        foreach ($users as $key => $value)
        {
            array_push($labels, $key);
            array_push($values, $value);
        }

        $charts = [
            [
                'labels' => $labels,
                'name'  => 'Some Data',
                'values' => $values,
            ]
        ];

        
       
        return [
            'users' => $charts,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            DynamicsOfRegistrations::class
        ];
    }
}
