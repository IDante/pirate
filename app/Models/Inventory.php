<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'week_price',
        'day_price',
        'rent_start_time',
        'rent_end_time',
        'free', 
        'employee_id'
    ];


}
