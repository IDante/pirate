<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'money',
        'user_id'
    ]; 



    protected $attributes = [
        'money' => "{}",
    ];

    protected $casts = [
        'money' => 'array',
    ];

    public function rents()
    {

        return $this->hasMany(Rent::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
