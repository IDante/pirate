<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    use HasFactory;

    protected $fillable = [
        'inventory_id',
        'client_id',
        'employee_id',
        'price',
        'rent_end_time',
        'rent_start_time',
        'id'
    ];
    
}
