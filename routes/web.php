<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\InventoryController::class, 'get_all'])->name('home');


Route::group(['prefix' => 'rent'], function ($router) {
  
    Route::post('/store', 'App\Http\Controllers\RentController@store')->name('rent.store');
    Route::get('/create/{id}', 'App\Http\Controllers\RentController@create')->name('rent.create');
    Route::delete('/delete/{id}', 'App\Http\Controllers\RentController@delete')->name('rent.delete');
   // Route::post('/search/{limit}', 'InventoryController@search');
});


Route::group(['prefix' => 'inventory'], function ($router) {
  
    Route::get('/create', 'App\Http\Controllers\InventoryController@create')->name('inventory.create');
    Route::post('/store', 'App\Http\Controllers\InventoryController@store')->name('inventory.store');
    Route::post('/edit/{id}', 'App\Http\Controllers\InventoryController@Edit')->name('inventory.edit');
    Route::get('/edits{id}', 'App\Http\Controllers\InventoryController@Edit_s')->name('inventory.edits');
    Route::get('/makeemp/{id}', 'App\Http\Controllers\InventoryController@make_emp')->name('makeemp');
    Route::delete('/delete/{id}', 'App\Http\Controllers\InventoryController@delete')->name('inventory.delete');
    Route::get('/getall/{status}', 'App\Http\Controllers\InventoryController@get_all')->name('inventory.getall');
   // Route::post('/search/{limit}', 'InventoryController@search');

});

Route::get('/statistics', 'App\Http\Controllers\InventoryController@statistics')->name('statistics');
Route::get('/search', 'App\Http\Controllers\InventoryController@search')->name('search');

Route::get('/wow', function() 
{
    Auth::user()->addRole('admin');
});

Route::get('ajax',function() {
    return view('message');
 });
 Route::post('/getmsg','AjaxController@index');