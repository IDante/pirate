@extends('layouts.app')
{{  
Form::macro('myField', function()
{
    return '<div class="form-group">
						<label for="message">Описание</label>
						<textarea name="message" class="form-control">	
						
						</textarea>
					</div>';
});
}}
@section('content')
	<div class="container">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{$errors->first()}}
			</div>
		@endif

		@if(session('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{session()->get('success')}}
			</div>
		@endif
		<h2> Добавление инвентаря</h2>
		
		<div class="col-md-12 bg-white row">	
			{{Form::open(['route' => 'inventory.store'])}}
			<table class="table">
				<tbody>
				  
				  <tr>
					<th scope="row"></th>
					<td>Name</td>
					<td>Day_price</td>
					<td>Week_price</td>
				  </tr>
				  <tr>
					<th scope="row"></th>
					<td>{{Form::text('name')}}</td>
					<td>{{Form::number('day_price')}}</td>
					<td>{{Form::number('week_price')}}</td>
				  </tr>
				</tbody>
			  </table>
			
				
			
    			
				
				
				
   				{{Form::submit('Save')}}
			{{Form::close()}}
			<div class="card-body col-xs-6">
				<div class="container-fluid">
					
				</div>
			</div>
		</div>
	</div>
@endsection