@extends('layouts.app')




@section('content')
	<div class="container">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{$errors->first()}}
			</div>
		@endif
		@if(session('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{session()->get('success')}}
			</div>
		@endif
		<h3>Редактирование инвентаря</h3>
		<div class="col-md-12 bg-white row">	
			{{Form::open(['route' => ['inventory.edits',$item->id]])}}
			<table class="table">
				<tbody>
				  
				  <tr>
					<th scope="row"></th>
					<td>Name</td>
					<td>Day_price</td>
					<td>Week_price</td>
				  </tr>
				  <tr>
					<th scope="row"></th>
					<td>{{Form::text('name', $item->name)}}</td>
					<td>{{Form::number('week_price',  $item->week_price)}}</td>
					<td>{{Form::number('day_price', $item->day_price)}}</td>
				  </tr>
				</tbody>
			  </table>
				
   				{{Form::submit('Save')}}
			{{Form::close()}}

			<h4>Занято</h4>
			<table class="table table-bordered">
			  <thead>
				<tr>
				  <th scope="col">From</th>
				  <th scope="col">To</th>
				</tr>
			  </thead>
			  <tbody>
				
				  @foreach ($dates as $item)
				  <tr>
					  <th scope="row">{{  $item->rent_start_time  }}</th>
					  <td>{{  $item->rent_end_time  }}</td>
					  <td> 

						  
						  <div class="d-flex justify-content-end">
							  <form action="{{ route('rent.delete', $item->id) }}" method="post">
								  @csrf
								  @method('DELETE')
								  <div class="btn-group">
									  
									  <button type="submit" class="btn btn-danger btn-sm">Delete rent</button>
								  </div>
								  </form>
							  </div>
						  
					  
					  </td>
				  </tr>
				  @endforeach
				
				
			  </tbody>
			</table>
			
			<div class="card-body col-xs-6">
				<div class="container-fluid">
					
				</div>
			</div>
		</div>
	</div>
@endsection