@extends('layouts.app')
@section('content')
<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Список статей</title>

<!-- Bootstrap -->

<link href="/css/app.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->

</head>

<body>

<div class="container">

<h1>Список статей</h1>

<div class='row'>


<a href="{{ url('inventory/create') }}"  class="btn btn-primary btn-lg pull-right">Добавить инвентарь</a>

</div>

<br />

<div class='row @if(count($articles)!= 0) show @else hidden @endif' id='articles-wrap'>

<table class="table table-striped ">

<thead>

<tr>


<th>Название</th>

<th>Цена за день</th>

<th>Цена за неделю</th>

<th></th>

</tr>

</thead>

<tbody>

@foreach($articles as $article)

<tr>



<td>{{ $article->name }}</a></td>

<td>{{ $article->day_price }}</a></td>

<td>{{ $article->week_price }}</a></td>



@if ( $article->free == true)
    <td> free </td>
@else
    <td> booked</td>
@endif




<td>
    

    <div class="d-flex justify-content-end">
    <form action="{{ route('inventory.delete', $article->id) }}" method="post">
        @csrf
        @method('DELETE')
        <div class="btn-group">
            <a href="{{route('inventory.edits', $article->id)}}" class="btn btn-info btn-sm">Edit</a>
            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
        </div>
        </form>
    </div>

</td>





</tr>

@endforeach

</tbody>

</table>

</div>

<div class="row">
    @if (count($articles) == 0)
    <div class="alert alert-warning" role="alert">Нет записей</div>
@endif

</div>

</div>


@endsection
