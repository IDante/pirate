@extends('layouts.app')
{{  
Form::macro('myField', function()
{
    return '<div class="form-group">
		<button type="submit" class="btn btn-primary">Сохранить</button>
	</div>';
});
}}
@section('content')
	<div class="container">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{$errors->first()}}
			</div>
		@endif
		@if(session('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				{{session()->get('success')}}
			</div>
		@endif
		<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			  @if(Session::has('alert-' . $msg))
		
			  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  @endif
			@endforeach
		  </div> 
		<h3>Аренда инвентаря</h3>
		<div class="col-md-12 bg-white row">	
			
			<div class="card-body col-xs-6">
				<div class="container-fluid">

					<div class="form-group">
						<label for="title">Название:</label>					
						<input type="text" name="id" value="{{$item->name}}" class="form-control" disabled />
					</div>
					<div class="form-group">
						<label for="created_at">Стоимость за день</label>
						<input type="text" name="created_at" value="{{$item->day_price}}" class="form-control" disabled />
					</div>
					<div class="form-group">
						<label for="updated_at">Стоимость за неделю</label>
						<input type="text" name="updated_at" value="{{$item->week_price}}" class="form-control" disabled />
					</div>
					
				</div>
			</div>
			{{Form::open(['route' => ['rent.store', ['inventory_id' => $item->id]]])}}
    			
			<table class="table">
				<tbody>
				  
				  <tr>
					<th scope="row"></th>
					<td>Date from</td>
					<td>Date to</td>
					
				  </tr>
				  <tr>
					<th scope="row"></th>
					<td>{{Form::text('from', '', array('id' => 'datepicker'))}}</td>
					<td>{{Form::text('to', '', array('id' => 'datepicker2'))}}</td>
					<td>{{Form::submit('Rent')}}</td>
					{{Form::close()}}
				  </tr>
				</tbody>
			</table>

			@if($dates != null)


			  <h4>Занято</h4>
			  <table class="table table-bordered">
				<thead>
				  <tr>
					<th scope="col">From</th>
					<th scope="col">To</th>
				  </tr>
				</thead>
				<tbody>
				 
					@foreach ($dates as $item)
					<tr>
						<th scope="row">{{  $item->rent_start_time  }}</th>
						<td>{{  $item->rent_end_time  }}</td>
						<td> 

							
							
						
						</td>
					</tr>
					@endforeach
				  
				  
				</tbody>
			  </table>
			  @endif

			
   				
				
				
				
		
		</div>
	</div>
	<footer>   
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<script>
		$(function() {
		  $( "#datepicker" ).datepicker();
		});
		</script>

		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<script>
		$(function() {
		$( "#datepicker2" ).datepicker();
		});
		</script>
	</footer>
@endsection

