@extends('layouts.app')
@section('content')

<form action="{{ route('search') }}" method="get">
    <input name="name" placeholder="Искать здесь..." type="search">
    <button type="submit">Поиск</button>
  </form>


<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Работник</th>
        <th scope="col">Доход</th>
      </tr>
    </thead>
    <tbody>
      
        @foreach ($users as $user)
        <tr>
            <th scope="row">{{  $user['user']->name  }}</th>
            <td>{{ $user['money'] }}</td>
        </tr>
        @endforeach
      
      
    </tbody>
  </table>




@endsection