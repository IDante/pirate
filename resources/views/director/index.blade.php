@extends('layouts.app')
@section('content')
<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Список статей</title>

<!-- Bootstrap -->

<link href="/css/app.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->

</head>

<body>

<div class="container">

<h1>Инвентарь</h1>

<div class='row'>


</div>

<br />

<div class='row @if(count($articles)!= 0) show @else hidden @endif' id='articles-wrap'>
    <select class="form-control form-control-lg" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"> 
        <option value="{{route('inventory.getall', 'all')}}">Choose</option> 
        <option value="{{route('inventory.getall', 'all')}}">All</option> 
        <option value="{{route('inventory.getall', 'free')}}">Free</option> 
        
    </select> 

<table class="table table-striped ">

<thead>

<tr>


<th>Название</th>


<th>Цена за день</th>

<th>Цена за неделю</th>
<th></th>

</tr>

</thead>

<tbody>

@foreach($articles as $article)

<tr>


@if($article->free == true)
<td><a href="{{ route('rent.create', $article->id) }}">{{ $article->name }}</a></td>
@else
<td><a>{{ $article->name }}</a></td>
@endif

<td>{{ $article->day_price }}</a></td>

<td>{{ $article->week_price }}</a></td>


@if ( $article->free == true)
    <td> free </td>
@else
    <td> booked</td>
@endif

</tr>

@endforeach

</tbody>

</table>

</div>

<div class="d-flex justify-content-between">
<div class="col">
        @if (count($articles) == 0)
            <div class="alert alert-warning" role="alert">Нет записей</div>
        @endif
</div>
<div class="d-flex justify-content-around">
    
        <a href="{{ route('show') }}"  class="btn btn-primary btn-lg pull-right">График</a>
        <a href="{{ route('statistics') }}"  class="btn btn-primary btn-lg pull-right">Статистика</a>
    
</div>


</div>

</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="{{ asset('js/bootstrap.min.js') }}"></script>

</body>

</html>
@endsection